// You should implement the following function:

fn sum_of_multiples(number: i32, multiple1: i32, multiple2: i32) -> i32 
{
    let mut sum: i32 = 0;
    let mut m1: i32 = multiple1;
    let mut m2: i32 = multiple2;

    while m1 < number {
        sum += m1;
        m1 += multiple1;
    }

    while m2 < number {
        // avoid dupes from the prev loop
        if m2 % multiple1 != 0 {
            sum += m2;
        }
        m2 += multiple2;
    }
    
    sum
}

fn main() {
    println!("{} should be 233168", sum_of_multiples(1000, 5, 3));
    assert!(sum_of_multiples(1000, 5, 3) == 233168)
}
